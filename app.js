#!/usr/bin/env node
'use strict'

require('dotenv').config()
const program = require('commander');
const Avanza = require('avanza')

program
  .version('1.1.0')

program
    .command('get')
    .alias('g')
    .description('Get data from avanza')
    .action(function(cmd, options){
        const avanza = new Avanza()

        avanza.authenticate({
            username: process.env.AVANZA_USERNAME,
            password: process.env.AVANZA_PASSWORD
        }).then(async () => {
            const positions = await avanza.getPositions();
            const stockCategory = positions.instrumentPositions[0];
            console.log("Total value:"+stockCategory.totalValue);
            console.log("Total own capital: "+positions.totalOwnCapital);
            stockCategory.positions.forEach(function(stock) {
                console.log(stock.name);
            });
            process.exit(0);
        })
    }).on('--help', function() {		
        console.log();		
        console.log('  Examples:');		
        console.log();		
        console.log('    $ nza get 132132');		
        console.log();		
    });

program
    .command('subscribe <stockId>')
    .alias('sub')
    .description('Subscribe to stock data from avanza')
    .action(function(cmd, options){
        console.log("Fetching data for: "+cmd);
        const avanza = new Avanza()

        avanza.authenticate({
            username: process.env.AVANZA_USERNAME,
            password: process.env.AVANZA_PASSWORD
        }).then(async () => {
            avanza.subscribe(Avanza.QUOTES, cmd, (quote) => {
                console.log('Received quote:', quote)
            });
        });
    }).on('--help', function() {		
        console.log();		
        console.log('  Examples:');		
        console.log();		
        console.log('    $ nza get 132132');		
        console.log();		
    });

program
    .command('search <query>')
    .alias('s')
    .description('Search stocks on avanza')
    .action(function(cmd, options){
        const avanza = new Avanza()

        avanza.authenticate({
            username: process.env.AVANZA_USERNAME,
            password: process.env.AVANZA_PASSWORD
        }).then(async () => {
            const result = await avanza.search(cmd);
            console.log(result);
            console.log(result.hits[0].topHits);
            process.exit(0);
        });
    }).on('--help', function() {		
        console.log();		
        console.log('  Examples:');		
        console.log();		
        console.log('    $ nza get 132132');		
        console.log();		
    });

program
    .command('scrape')
    .alias('sc')
    .description('Search stocks on avanza')
    .action(function(cmd, options){
        //const winnersAndLoosers = require('./winners-and-loosers.js')
        //winnersAndLoosers.get(winnersAndLoosers.TIMEUNIT.ONE_WEEK);

        const themeLists = require('./theme-lists.js')
        themeLists.get();
    }).on('--help', function() {		
        console.log();		
        console.log('  Examples:');		
        console.log();		
        console.log('    $ nza get 132132');		
        console.log();		
    });

if (!process.argv.slice(2).length) {
    program.outputHelp();
}

program.parse(process.argv);
