'use strict'
const scrapeIt = require("scrape-it");

const TIMEUNIT= {
    TODAY : "TODAY",
    ONE_WEEK : "ONE_WEEK",
    ONE_MONTH : "ONE_MONTH",

}

exports.TIMEUNIT = TIMEUNIT

exports.get = (timeUnit) => {
    scrapeIt("https://www.avanza.se/aktier/vinnare-forlorare.html?countryCode=SE&listKeys=LargeCap.SE&timeUnit="+timeUnit, {
        title: "#contentTableWrapper .tRight .instrumentName .link",
        percentage: "#contentTableWrapper .tRight .changePercent"
    }).then(page => {
        page.title = page.title.replace(/(\r)/gm,"")
        page.title = page.title.replace(/ {2,10}/g, '')
        console.log(page);

        var allTitles = page.title.split('\n')
        //console.log(allTitles)
        console.log(page.percentage.length)
        var percents = [];
        var percent = "";
        var count = 0;
        var maxChars = 4;
        for (var i = 0; i <= page.percentage.length; i++) {
            if (count === maxChars) {
                count = 0;
                percents.push(percent);
                percent = "";
            }
            if (page.percentage[i] === '-') {
                percent += "-";
            } else {
                count++;
                percent += page.percentage[i];
            }
        }
        console.log(allTitles.map(function(title, i) {
            return {"title": title, "percentage": percents[i], "timeUnit": timeUnit}
        }))
    });
}
