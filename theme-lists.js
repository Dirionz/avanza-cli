'use strict'
const scrapeIt = require("scrape-it");

exports.get = () => {
    scrapeIt("https://www.avanza.se/aktier/temalistor.html", {
        title: ".inspirationListCollection .inspirationListSummary .listTitle",
        today: ".inspirationListCollection .inspirationListSummary .today",
        threemonths: ".inspirationListCollection .inspirationListSummary .threemonths",
        oneyear: ".inspirationListCollection .inspirationListSummary .oneyear",
        threeyears: ".inspirationListCollection .inspirationListSummary .threeyears",
    }).then(page => {
        page.title = page.title.replace(/(\r|\t)/gm,"")
        page.title = page.title.replace(/(\n)+/gm,"\n")
        page.today = page.today.replace(/(\r|\t)/gm,"")
        page.today = page.today.replace(/(\n)+/gm,"\n")
        page.today = page.today.replace(/(Idag\n)/gm,"")
        page.threemonths = page.threemonths.replace(/(\r|\t)/gm,"")
        page.threemonths = page.threemonths.replace(/(\n)+/gm,"\n")
        page.threemonths = page.threemonths.replace(/(3 mån.\n)/gm,"")
        page.oneyear = page.oneyear.replace(/(\r|\t)/gm,"")
        page.oneyear = page.oneyear.replace(/(\n)+/gm,"\n")
        page.oneyear = page.oneyear.replace(/(1 år\n)/gm,"")
        page.threeyears = page.threeyears.replace(/(\r|\t)/gm,"")
        page.threeyears = page.threeyears.replace(/(\n)+/gm,"\n")
        page.threeyears = page.threeyears.replace(/(3 år\n)/gm,"")
        console.log(page);

        var allTitles = page.title.split('\n')
        console.log(allTitles)

        var allToday = page.today.split('\n')
        console.log(allToday)

        var allThreemonths = page.threemonths.split('\n')
        console.log(allThreemonths)

        var allOneyear = page.oneyear.split('\n')
        console.log(allOneyear)

        var allThreeyears = page.threeyears.split('\n')
        console.log(allThreeyears)

        console.log(allTitles.map(function(title, i) {
            return {"title": title, "today": allToday[i], "threemonths": allThreemonths[i], 
                    "oneyear": allOneyear[i], "threeyears": allThreeyears[i]}
        }))
    });
}
